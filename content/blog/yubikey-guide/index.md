---
title: YubiKey 使用指南
date: "2022-05-07T05:11:59.580Z"
description: This is a custom description for SEO and Open Graph purposes, rather than the default generated excerpt. Simply add a description field to the frontmatter.
---

## GPG 智能卡

生成新密钥：

```bash
gpg --expert --full-generate-key
gpg --expert --edit-key E39AE6ADD4A2EB05
#> addkey
```

查看 GPG 智能卡状态：

```bash
gpg --card-status
```

签名前要求触碰：

```bash
ykman openpgp keys set-touch aut on
```

## FIDO 2

```bash
ssh-keygen -t ed25519-sk -O resident
```
